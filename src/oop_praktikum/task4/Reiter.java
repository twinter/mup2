package oop_praktikum.task4;

import java.util.List;

/**
 * Datenklasse für Reiter
 */
class Reiter {
	/**
	 * Name des Reiters
	 */
	String name;

	/**
	 * Skilllevel des Reiters
	 */
	Integer koennen;

	/**
	 * Liste mit Wunschpferden
	 */
	List<Pferd> wunschpferde;

	/**
	 * Konstruktor, füllt die Eiugenschaften der Klasse mit Werten
	 *
	 * @param name Name des Reiters
	 * @param koennen Skilllevel des Reiters
	 * @param wunschpferde Liste mit gewünschten Pferden
	 */
	Reiter(String name, Integer koennen, List<Pferd> wunschpferde) {
		this.name = name;
		this.koennen = koennen;
		this.wunschpferde = wunschpferde;
	}
}
