package oop_praktikum.task4;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Klasse zum Pairing von Reitern mit Pferden
 */
class Reiterhof {
	/**
	 * Array mit den verfügbaren Pferden
	 */
	private Pferd[] pferde;

	/**
	 * Array mit verfügbaren Reitern
	 */
	private Reiter[] reiter;

	/**
	 * Konstruktor, füllt die Listen für Reiter und Pferde
	 *
	 * @param pferde die zu nutzenden Pferde
	 * @param reiter die zu nutzenden Reiter
	 */
	Reiterhof(Pferd[] pferde, Reiter[] reiter) {
		this.pferde = pferde;
		this.reiter = reiter;
	}

	/**
	 * Funktion um Reiter zu Pferden zuzuordnen. Setzt mehr Pferde als Reiter vorraus.
	 * Versucht möglichst viele 1 auf der Hauptdiagonale zu ordnen, erzeugt Pairing
	 *
	 * @return die Zurodnung von Reitern zu Pferden
	 */
	HashMap<Reiter, Pferd> ordneZu() {
		// sortiere Reiter und Pferde (geringstes Können zuerst)
		Arrays.sort(this.reiter, Comparator.comparingInt(reiter -> reiter.koennen));
		Arrays.sort(this.pferde, Comparator.comparingInt(pferd -> pferd.schwierigkeit));

		// erstelle Matrix und Indizes ([Pferd][Reiter])
		boolean[][] optionen = new boolean[this.pferde.length][this.reiter.length];
		for (int i = 0; i < this.pferde.length; i++) {
			for (int j = 0; j < this.reiter.length; j++) {
				if (this.reiter[j].wunschpferde.size() == 0)
					optionen[i][j] = true;
				else
					optionen[i][j] = this.reiter[j].wunschpferde.contains(this.pferde[i]);
			}
		}

		optionen = this.pairingImprove(optionen);

		// erstelle HashMap und return
		int iMax = Math.min(this.reiter.length, this.pferde.length);
		HashMap<Reiter, Pferd> ret = new HashMap<>();
		for (int i = 0; i < iMax; i++)
			ret.put(this.reiter[i], this.pferde[i]);
		return ret;
	}

	/**
	 * Optimiert die Zuordnung. Nimmt an, dass es mehr Pferde als Reiter gibt.
	 * Tauscht die Zeilen um die Anzahl der 1en auf der Hauptdiagonalen zu erhöhen.
	 *
	 * @param optionen die Optionsmatrix der Wünsche
	 * @return verbesserte Optionsmatrix
	 */
	private boolean[][] pairingImprove(boolean[][] optionen) {
		int iMax = Math.min(this.reiter.length, this.pferde.length);
		for (int i = 0; i < iMax; i++) {
			if (!optionen[i][i]) {
				// suche nach passendem Pferd zum Tausch (optionen[i] mit optionen[j])
				for (int j = 0; j < this.pferde.length; j++) {
					if (!optionen[j][i])
						continue;

					// überspringe wenn Tausch ein gültiges Paar entfernen würde
					if (j < iMax && optionen[j][j] && !optionen[i][j])
						continue;

					// teste ob Reiter und Pferd kompatibel
					if (this.pferde[j].schwierigkeit > this.reiter[i].koennen)
						continue;

					// tausche Zeile und Optionsmatrix und Eintrag in this.pferde
					boolean[] tmpBool = optionen[i];
					optionen[i] = optionen[j];
					optionen[j] = tmpBool;
					Pferd tmpPferd = this.pferde[i];
					this.pferde[i] = this.pferde[j];
					this.pferde[j] = tmpPferd;

					return pairingImprove(optionen);
				}
			}
		}

		return optionen;
	}
}
