package oop_praktikum.task4;

import java.util.HashMap;
import java.util.Map;

class Ausgabe {
	/**
	 * zuorndunf :hashmap zur zuordnung
	 *
	 */
	private HashMap<Reiter, Pferd> zuordnung;
	/**
	 * konstruktur
	 *@param zuordnung (vergleichung) vom typ hashmap
	 *
	 */
	Ausgabe(HashMap<Reiter, Pferd> zuordnung) {
		this.zuordnung = zuordnung;
	}
	/**
	 * in der konsole ausgeben
	 *
	 *
	 */
	void ausgabeKonsole() {
		for (Map.Entry<Reiter, Pferd> e : zuordnung.entrySet()) {
			Reiter key = e.getKey();
			Pferd value = e.getValue();
			System.out.println(key.name + ":" + value.name);
		}
	}
}
