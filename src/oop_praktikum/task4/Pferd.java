package oop_praktikum.task4;

/**
 * Datenklasse für Pferde
 */
class Pferd {
	/**
	 * Name des Pferdes
	 */
	String name;

	/**
	 * Schwierigkeitslevel des Pferdes
	 */
	Integer schwierigkeit;

	/**
	 * Konstruktor, füllt die Eigenschaften mit Werten
	 *
	 * @param name Name des Pferdes
	 * @param schwierigkeit Level des Pferdes
	 */
	Pferd(String name, Integer schwierigkeit) {
		this.name = name;
		this.schwierigkeit = schwierigkeit;
	}
}
