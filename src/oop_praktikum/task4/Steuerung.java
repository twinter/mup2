package oop_praktikum.task4;

import java.io.IOException;
import java.util.HashMap;

/**
 * Hauptklasse, steuert den Rest
 */
class Steuerung {
	/**
	 * Haupfunktion, steuert den Ablauf des Programms
	 */
	void start() {
		Eingabe eingabe = new Eingabe();

		Pferd[] pferde;
		Reiter[] reiter;
		try {
			pferde = eingabe.getPferde();
			reiter = eingabe.getReiter(pferde);
		} catch (IOException e) {
			System.out.println("Fehler bei Dateizugriff. Existiert die Datei?");
			return;
		}

		Reiterhof reiterhof = new Reiterhof(pferde, reiter);
		HashMap<Reiter, Pferd> zuordung = reiterhof.ordneZu();

		Ausgabe ausgabe = new Ausgabe(zuordung);
		ausgabe.ausgabeKonsole();
	}
}
