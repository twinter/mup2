package oop_praktikum.task4;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * eingeben der datei
 * lesen der detei
 * lesen jede zeil
 * in array speichern
 */
class Eingabe {
	/**
	 * pfadbasis ist pfad der datei
	 * br ist eingabe der text vom benutzer zum lesen
	 */
	private String pfadBasis;
	/**
	 * hier steht nur etwas um fehler zu vermeiden
	 */
	private BufferedReader br;
	/**
	 * Konstruktor.
	 *
	 * ein datei zulesen
	 */
	Eingabe() {
		this.br = new BufferedReader(new InputStreamReader(System.in));
		this.pfadBasis = Paths.get("").toAbsolutePath().toString() + "/src/oop_praktikum/task4/";
	}
	/**
	 * einfach ,damit wir Pferde bekommen,Datentyp ist list vom pferd
	 *
	 * @throws IOException bei dateifehlern
	 * @return liste mit pferden
	 */
	Pferd[] getPferde() throws IOException {
		Pattern pattern = Pattern.compile("(\\w+),([1-3])");

		// lade Dateipfad
		System.out.println("Name der csv-Datei mit den Pferden (ohne .csv am Ende) eingeben");
		String pfad = this.br.readLine() + ".csv";
		pfad = this.pfadBasis + pfad;
		BufferedReader fileReader = new BufferedReader(new FileReader(pfad));

		// lesen aus Datei
		ArrayList<Pferd> pferde = new ArrayList<>();
		while (true) {
			String zeile = fileReader.readLine();

			if (zeile == null)
				return pferde.toArray(new Pferd[0]);

			Matcher m = pattern.matcher(zeile);
			if (m.find()) {
				String name = m.group(1);
				int schwierigkeit = Integer.parseInt(m.group(2));
				Pferd p = new Pferd(name, schwierigkeit);
				pferde.add(p);
			}
		}
	}
	/**
	 * einfach ,Reiter zu bekommen,.
	 *
	 * @throws IOException bei dateifehlern
	 * @param pferde der pferde list
	 * @return liste mit reitern
	 */
	Reiter[] getReiter(Pferd[] pferde) throws IOException {
		Pattern pattern = Pattern.compile("(\\w+),([1-3]),\\[(:?(\\w+),?)*]");

		// lade Dateipfad
		System.out.println("Name der csv-Datei mit den Reitern (ohne .csv am Ende) eingeben");
		String pfad = this.br.readLine() + ".csv";
		pfad = this.pfadBasis + pfad;
		BufferedReader fileReader = new BufferedReader(new FileReader(pfad));

		ArrayList<Reiter> reiter = new ArrayList<>();
		while (true) {
			String zeile = fileReader.readLine();

			if (zeile == null)
				return reiter.toArray(new Reiter[0]);

			Matcher m = pattern.matcher(zeile);
			if (m.find()) {
				String name = m.group(1);
				int koennen = Integer.parseInt(m.group(2));
				List<Pferd> wunschpferde = new ArrayList<>();
				for (int i = 3; i <= m.groupCount(); i++)
					for (Pferd pferd : pferde) {
						if (pferd.name.equals(m.group(i))) {
							wunschpferde.add(pferd);
							break;
						}
					}
				Reiter r = new Reiter(name, koennen, wunschpferde);
				reiter.add(r);
			}
		}
	}
}
