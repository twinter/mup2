package oop_praktikum.task4;

/**
 * Main-Klasse
 */
public class Main {
	/**
	 * Main-Funktion
	 *
	 * @param args String-Array mit Argumenten
	 */
	public static void main(String[] args) {
		Steuerung steuerung = new Steuerung();
		steuerung.start();
	}
}
