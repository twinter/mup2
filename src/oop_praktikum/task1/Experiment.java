package oop_praktikum.task1;

/**
 * The object containing the experiment.
 */
class Experiment {

	/**
	 * The array containing the Dice objects used in the experiment.
	 */
	Dice[] dice;

	/**
	 * The default constructor.
	 */
	Experiment () {
		this.dice = new Dice[3];
		for (int i = 0; i < dice.length; i++)
			dice[i] = new Dice();
	}

	/**
	 * Run the experiment.
	 *
	 * @param rolls the amount of rolls we simulate
	 */
	void run(int rolls) {
		// roll the dice
		int hit_2 = 0;
		int hit_3 = 0;
		for (int i = 0; i < rolls; i++) {
			for (Dice d : dice) d.roll();

			// check for hits
			if (dice[0].last_roll == dice[1].last_roll && dice[1].last_roll == dice[2].last_roll) {
				hit_3++;
				hit_2++;
			} else if (
				dice[0].last_roll == dice[1].last_roll
					|| dice[1].last_roll == dice[2].last_roll
					|| dice[2].last_roll == dice[0].last_roll
				)
				hit_2++;

			// progress indicator
			if (i % 100000 == 0 && i != 0)
				System.out.println(Integer.toString(i) + " Würfe simuliert");
		}

		Experiment.evaluate(rolls, 3, hit_3, dice[0].faces);
		Experiment.evaluate(rolls, 2, hit_2, dice[0].faces);
	}

	/**
	 * Evaluate the result of the experiment and print the result to the console.
	 *
	 * @param rolls how many rolls/rounds we simulated
	 * @param same_dice amount of dice wit identical results in the experiment
	 * @param hits amount of rolls with the above amount of identical dice
	 * @param faces the amount of faces of a dice
	 */
	private static void evaluate(int rolls, int same_dice, int hits, int faces) {
		double probability_real = (double) hits / (double) rolls;
		double probability_theoretical = Math.pow((1. / (double) faces), same_dice);
		double error_abolute = probability_real - probability_theoretical;
		double error_relative = Math.abs(error_abolute / probability_theoretical);

		System.out.println(
			Integer.toString(same_dice) + " Würfel:\n"
				+ "simulierte Würfe: " + Integer.toString(rolls) + "\n"
				+ "ermittelte Ereignisse: " + Integer.toString(hits) + "\n"
				+ "ermittelte Wahrscheinlichkeit: " + Double.toString(probability_real) + "\n"
				+ "theoretische Wahrscheinlichkeit: " + Double.toString(probability_theoretical) + "\n"
				+ "relativer Fehler: " + Double.toString(error_relative) + "\n"
		);
	}
}
