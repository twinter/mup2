package oop_praktikum.task1;

import java.util.Random;

/**
 * A dice used in the experiment.
 */
class Dice {
	/**
	 * how many faces the dice has
	 */
	int faces = 9;

	/**
	 * The result of the last roll. 0 if we never rolled.
	 */
	int last_roll = 0;

	/**
	 * The Random object used to generate random numbers.
	 */
	private Random rand = new Random();

	/**
	 * The default constructor for a 9 sided dice.
	 */
	Dice() {}

	/**
	 * The constructor for any dice.
	 *
	 * @param faces how many faces the dice should have.
	 */
	Dice(int faces) {
		this.faces = faces;
	}

	/**
	 * Simulate a roll fo the dice. Saves the last value in last_value
	 *
	 * @return the result of the simulated throw
	 */
	int roll() {
		this.last_roll = this.rand.nextInt(this.faces) + 1;
		return this.last_roll;
	}
}
