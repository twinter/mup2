package oop_praktikum.task1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * The main class. Contains the I/O logic and starts the experiments.
 */
public class Main {

	/**
	 * The main function. Point of Entry to the program.
	 *
	 * @param args arguments passed to the program
	 */
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Experiment experiment = new Experiment();

		while (true) {
			// do console stuff
			System.out.println("Wie viele Würfe simulieren? 0 oder ungültiger Input beendet das Programm");

			// convert and/or terminate
			int rolls;
			try {
				rolls = Integer.parseInt(br.readLine());
			} catch (Exception e) {
				return;
			}

			if (rolls == 0)
				break;

			experiment.run(rolls);
		}

	}
}
