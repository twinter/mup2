package oop_praktikum.task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Klasse für den Förster. Enthält Funktionen zur Sichtbarkeit von Bäumen.
 */
class Foerster extends Waldobjekt {

	/**
	 * Referenz auf das Gitter Objekt.
	 */
	private Gitter gitter;

	/**
	 * Konstruktor.
	 *
	 * @param pos_x x Koordinate
	 * @param pos_y y Koordinate
	 * @param gitter Referenz auf das Gitter-Objekt
	 */
	Foerster(int pos_x, int pos_y, Gitter gitter) {
		super(pos_x, pos_y);
		this.gitter = gitter;
	}

	/**
	 * Erzeugt eine Liste der sichtbaren Bäume.
	 *
	 * @return Liste mit allen Bäumen die von der Position des Försters sichtbar sind
	 */
	List<Baum> get_sichtbare_baeume() {
		HashMap<String, Baum> baeume = new HashMap<>();

		for (int x = 0; x < this.gitter.spielfeld.length; x++) {
			for (int y = 0; y < this.gitter.spielfeld[x].length; y++) {
				// skip wenn kein Baum
				if (!this.gitter.ist_baum(x, y))
					continue;

				// berechne Schlüssel für Hashmap (aus Winkel der Geraden Förster-Baum) und Entfernung des Baums zum Förster
				double dx = x - this.x;
				double dy = y - this.y;
				double phi;
				if (dx < 0 && dy < 0)
					phi = Math.atan(dy / dx) - Math.PI;
				else if (dx == 0 && dy < 0)
					phi = -1 * Math.PI / 2;
				else if (dx > 0)
					phi = Math.atan(dy / dx);
				else if (dx == 0 && dy > 0)
					phi = Math.PI / 2;
				else
					phi = Math.atan(dy / dx) + Math.PI;

				String key = String.format("%.3f", phi);
				double entfernung = this.entfernung_zu_baum(x, y);

				// skip wenn näherer Baum bereits bekannt
				if (baeume.containsKey(key)) {
					Waldobjekt baum = baeume.get(key);
					double baum_entfernung = this.entfernung_zu_baum(baum.x, baum.y);
					if (baum_entfernung <= entfernung)
						continue;
				}

				baeume.put(key, (Baum) this.gitter.spielfeld[x][y]);
			}
		}

		return new ArrayList<>(baeume.values());
	}

	/**
	 * Berechnet die Entfernung des Försters zu beliebigen Koordinaten.
	 *
	 * @param x x Koordinate des Baum
	 * @param y y Koordinate des Baum
	 * @return Entfernung zu den gegebenen Koordinaten
	 */
	private double entfernung_zu_baum(int x, int y) {
		return Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));
	}

	/**
	 * Berechnet wie viele Bäume von der aktuellen Position sichtbar sind.
	 *
	 * @return Anzahl der sichtbaren Bäume
	 */
	int baeume_zaehlen() {
		return this.get_sichtbare_baeume().size();
	}

	/**
	 * Gibt eine Tabelle mit Markierungen für die sichtbaren (O) und unsichtbaren (X) Bäume und den Förster (F) aus.
	 */
	void zeige_sichtbarkeitsmatrix() {
		List<Baum> baeume_sichtbar = this.get_sichtbare_baeume();
		StringBuilder ret = new StringBuilder();

		for (int x = 0; x < this.gitter.spielfeld.length; x++) {
			for (int y = 0; y < this.gitter.spielfeld[x].length; y++) {
				// Förster
				if (x == this.x && y == this.y) {
					ret.append("F ");
					continue;
				}

				// kein Baum
				if (!this.gitter.ist_baum(x, y)) {
					ret.append("  ");
					continue;
				}

				// lade Baum, teste ob sichtbar
				Baum baum = (Baum) this.gitter.spielfeld[x][y];
				if (baeume_sichtbar.contains(baum))
					ret.append("o ");
				else
					ret.append("x ");
			}
			ret.append('\n');
		}

		System.out.print(ret);
	}
}
