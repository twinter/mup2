package oop_praktikum.task2;

/**
 * Elternobjekt für Baum und Förster
 */
class Waldobjekt {
	/**
	 * Position des Objektes
	 */
	int x, y;

	/**
	 * Konstruktor für Waldobjekt
	 *
	 * @param pos_x x Koordinate
	 * @param pos_y y Koordinate
	 */
	Waldobjekt(int pos_x, int pos_y) {
		this.x = pos_x;
		this.y = pos_y;
	}
}
