package oop_praktikum.task2;

/**
 * Ein einzelner Baum
 */
class Baum extends Waldobjekt {
	/**
	 * Konstruktor für Baum
	 *
	 * @param pos_x x Koordinate
	 * @param pos_y y Koordinate
	 */
	Baum(int pos_x, int pos_y) {
		super(pos_x, pos_y);
	}
}
