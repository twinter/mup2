package oop_praktikum.task2;

import java.util.ArrayList;
import java.util.Random;

/**
 * Klasse für das Experiment
 */
class Experiment {
	Gitter gitter;

	/**
	 * Konstruktor für Experiement Klasse
	 *
	 * @param breite Breite des Spielfeldes
	 * @param hoehe Höhe des Spielfeldes
	 */
	Experiment(int breite, int hoehe) {
		this.gitter = new Gitter(breite, hoehe);
	}

	/**
	 * Startet das Experiment mit beiden Testserien
	 */
	void start() {
		this.serie1(10);
		this.serie2();
	}

	/**
	 * Testserie 1: n zufällig gewählte Positionen testen
	 *
	 * @param anzahl Anzahl der zufälligen Positionen
	 */
	private void serie1(int anzahl) {
		System.out.println("\n\n");
		System.out.println("----- Testserie 1 -----");
		Random r = new Random();
		Waldobjekt[] positionen = new Waldobjekt[anzahl];
		for (int i = 0; i < anzahl; i++) {
			int x = r.nextInt(this.gitter.spielfeld.length);
			int y = r.nextInt(this.gitter.spielfeld[x].length);
			positionen[i] = new Waldobjekt(x, y);
		}

		test_positionen(positionen);
	}

	/**
	 * Testserie 2: jede Postion testen
	 */
	private void serie2() {
		System.out.println("\n\n");
		System.out.println("----- Testserie 2 -----");
		int len_x = this.gitter.spielfeld.length;
		int len_y = this.gitter.spielfeld[0].length;
		Waldobjekt[] positionen = new Waldobjekt[len_x * len_y];
		for (int x = 0; x < len_x; x++) {
			for (int y = 0; y < len_y; y++) {
				positionen[x * len_y + y] = new Waldobjekt(x, y);
			}
		}

		this.test_positionen(positionen);
	}

	/**
	 * Testen eine Reihe gegebener Psotionen
	 *
	 * @param positionen Liste der zu testenden Psotionen
	 */
	private void test_positionen(Waldobjekt[] positionen) {
		int maximale_baeume = 0;
		ArrayList<Waldobjekt> maximale_positionen = new ArrayList<Waldobjekt>();

		for (Waldobjekt pos : positionen) {
			this.gitter.platziere_baeume();
			this.gitter.platziere_foerster(pos.x, pos.y);

			int sichtbare_baeume = this.gitter.foerster.baeume_zaehlen();

			if (sichtbare_baeume < maximale_baeume)
				continue;

			if (sichtbare_baeume > maximale_baeume) {
				maximale_positionen.clear();
				maximale_baeume = sichtbare_baeume;
			}

			maximale_positionen.add(pos);
		}

		System.out.println("Sichtbarkeitsmatrizen für " + Integer.toString(maximale_baeume) + " sichtbare Bäume");
		System.out.println("(" + Integer.toString(maximale_positionen.size()) + " Positionen)");

		for (Waldobjekt pos : maximale_positionen) {
			System.out.println();
			System.out.println("Position: " + Integer.toString(pos.x) + ", " + Integer.toString(pos.y));
			this.gitter.platziere_baeume();
			this.gitter.platziere_foerster(pos.x, pos.y);
			this.gitter.foerster.zeige_sichtbarkeitsmatrix();
		}
	}

}


