package oop_praktikum.task2;

/**
 * Klasse für das Spielfeld/Gitter auf dem Bäume und FÖrster platziert werden.
 */
class Gitter {

	/**
	 * Das Spielfeld
	 */
	Waldobjekt[][] spielfeld;

	/**
	 * Referenz auf das Förster Objekt
	 */
	Foerster foerster;

	/**
	 * Konstruktor für das Spielfeld
	 *
	 * @param breite Breite des Spielfeldes
	 * @param hoehe Höhe des Spielfeldes
	 */
	Gitter(int breite, int hoehe) {
		this.spielfeld = new Waldobjekt[breite][hoehe];
	}

	/**
	 * Fülle das Spielfeld mit Bäumen
	 */
	void platziere_baeume() {
		for (int i = 0; i < this.spielfeld.length; i++) {
			for (int j = 0; j < this.spielfeld[i].length; j++) {
				this.spielfeld[i][j] = new Baum(i, j);
			}
		}
	}

	/**
	 * Platziere den Förster auf einer bestimmten Position
	 *
	 * @param pos_x x Koordinate für den förster
	 * @param pos_y y Koordinate für den Förster
	 */
	void platziere_foerster(int pos_x, int pos_y) {
		this.foerster = new Foerster(pos_x, pos_y, this);
		this.spielfeld[pos_x][pos_y] = this.foerster;
	}

	/**
	 * Test ob auf einem gegebenen Feld ein Baum steht
	 *
	 * @param x x Koordinate
	 * @param y y Koordinate
	 * @return true wenn Baum, false wenn nicht
	 */
	boolean ist_baum(int x, int y) {
		return this.spielfeld[x][y] instanceof Baum;
	}

	/**
	 * Test ob auf einem gegebenen Feld ein Förster steht
	 *
	 * @param x x Koordinate
	 * @param y y Koordinate
	 * @return true wenn Förster, false wenn nicht
	 */
	boolean ist_foerster(int x, int y) {
		return this.spielfeld[x][y] instanceof Foerster;
	}

	/**
	 * Test ob ein gegebenes Feld leer ist
	 *
	 * @param x x Koordinate
	 * @param y y Koordinate
	 * @return true wenn Leer, false wenn nicht
	 */
	boolean ist_leer(int x, int y) {
		return this.spielfeld[x][y] == null;
	}


}