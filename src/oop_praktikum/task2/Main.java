package oop_praktikum.task2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Hauptklasse. Enthält einfache Eingabe der Spielfeldgröße und erzeugt das Experiment.
 */
public class Main {

	/**
	 * Einstiegspunkt für das Programm
	 *
	 * @param args beim Start übergebene Argument
	 */
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int breite, hoehe;
		try {
			System.out.println("Gibt die Breite des Spielfelds ein.");
			breite = Integer.parseInt(br.readLine());
			System.out.println("Gibt die Höhe des Spielfelds ein.");
			hoehe = Integer.parseInt(br.readLine());
		} catch (Exception e) {
			System.out.println("Ungültige Eingabe! Programm wird beendet.");
			return;
		}

		Experiment e = new Experiment(breite, hoehe);
		e.start();
	}
}
