package oop_praktikum.task3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Klasse für Dateieingabe
 */
class Eingabe {
	/**
	 * lädt text aus Datei unter gegebenem Pfad
	 *
	 * @param pfad Pfad der Datei mit Text
	 * @return den Text in der Datei oder "" bei Fehler
	 */
	String ladeText(String pfad) {
		// öffne Datei
		BufferedReader br_file = null;
		try {
			br_file = new BufferedReader(new FileReader(pfad));
		} catch (FileNotFoundException e) {
			System.out.println("Datei " + pfad + " nicht gefunden");
			return "";
		}

		// lese aus Datei
		StringBuilder text_string_builder = new StringBuilder();
		String zeile;
		try {
			while ((zeile = br_file.readLine()) != null)
				text_string_builder.append(zeile).append('\n');
		} catch (IOException e) {
			System.out.println("Lesen aus Datei nicht möglich");
		}

		return text_string_builder.toString();
	}
}
