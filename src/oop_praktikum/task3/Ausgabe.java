package oop_praktikum.task3;

import java.io.*;

/**
 * Speichert Text in einer Steuerung und gibt in der Konsole aus.
 */
class Ausgabe {
	/**
	 * auszugebender Text
	 */
	private String text;

	/**
	 * Konstruktor.
	 *
	 * @param text der Eingabetext
	 */
	Ausgabe(String text) {

		this.text = text;
	}

	/**
	 * text in konsole ausgeben
	 */
	void ausgabeKonsole() {
		System.out.print(text);
	}

	/**
	 * text ausgeben
	 *
	 * @param dateiname ist ein variable,in dem Eingabe datei-name gespeichert.
	 */
	void ausgabeDatei(String dateiname) {
		try {
			PrintWriter w = new PrintWriter(dateiname);
			w.print(text);
			w.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}






