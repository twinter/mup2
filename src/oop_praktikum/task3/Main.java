package oop_praktikum.task3;

/**
 * Hauptklasse
 */
public class Main {
	/**
	 * main-Methode
	 * @param args übergebene Argumente
	 */
	public static void main(String[] args) {
		Steuerung steuerung = new Steuerung();
		steuerung.start();
	}
}
