package oop_praktikum.task3;

import java.util.HashMap;
import java.util.Map;

/**
 * Ersetzt Sonderzeichen im Gegebenen Text
 */
class Ersetzer {
	/**
	 * zu bearbeitender Text
	 */
	private String text;

	/**
	 * Konstruktor
	 *
	 * @param text zu bearbeitender Text
	 */
	Ersetzer(String text) {
		this.text = text;
	}

	/**
	 * Umwandlung einige Zeichnen mit anderen zeichnen ,die im Tabelle_Aufgabe sind
	 *
	 * @param sonderzeichen HashMap mit zu ersetzenden Sonderzeichen
	 * @return Eingabetext mit Sonderzeichnen
	 */
	String ersetze(HashMap<String, String> sonderzeichen) {
		String text = this.text;
		for (Map.Entry<String, String> zeichen : sonderzeichen.entrySet()) {
			text = text.replaceAll(zeichen.getKey(), zeichen.getValue());
		}
		return text;
	}
}
