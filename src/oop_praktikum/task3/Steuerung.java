package oop_praktikum.task3;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Lädt die Steuerung vom gegebenen Pfad und gibt sie an den Rest weiter.
 */
class Steuerung {
	/**
	 * zu bearbeitender Text
	 */
	private String text;

	/**
	 * The buffered reader to read from the console
	 */
	private BufferedReader br;

	/**
	 * Basis-Pfad für Dateipfade
	 */
	private String pfad_basis;

	Steuerung() {
		this.br = new BufferedReader(new InputStreamReader(System.in));
		this.pfad_basis = Paths.get("").toAbsolutePath().toString() + "/src/oop_praktikum/task3/";
	}

	/**
	 * grundlegender Programmablauf
	 */
	void start() {
		String pfad_in;
		String pfad_out;

		try {
			pfad_in = this.ladePfadIn();

			this.ladeText(pfad_in);
			if (this.text.length() <= 0)
				return;

			pfad_out = this.ladePfadOut();
		} catch (IOException e) {
			System.out.println("Fehler bei Dateieingabe");
			return;
		}

		this.bearbeiteText();
		this.ausgabeText(pfad_out);
	}

	/**
	 * lädt Eingabepfad
	 *
	 * @return array mit Ein- und Ausgabepfad
	 * @throws IOException bei Lesefehler
	 */
	private String ladePfadIn() throws IOException {
		String pfad;

		System.out.println("Name der html-Steuerung (ohne .html)");
		pfad = br.readLine() + ".html";
		pfad = Paths.get(this.pfad_basis + pfad).toAbsolutePath().toString();

		return pfad;
	}

	/**
	 * lädt Ausgabepfad
	 *
	 * @return Ausgabepfad
	 * @throws IOException bei Lesefehler
	 */
	private String ladePfadOut() throws IOException {
		String pfad;

		System.out.println("Dateiname für die Ausgabe");
		pfad = this.br.readLine();
		pfad = Paths.get(this.pfad_basis + pfad).toAbsolutePath().toString();

		return pfad;
	}

	/**
	 * Lädt Text aus datei
	 *
	 * @param pfad_in Eingabepfad
	 */
	private void ladeText(String pfad_in) {
		Eingabe eingabe = new Eingabe();
		this.text = eingabe.ladeText(pfad_in);
	}

	private void bearbeiteText() {
		EntfernerTags entferner_tags = new EntfernerTags(this.text);
		this.text = entferner_tags.entferneTags();

		Ersetzer ersetzer_sonderzeichen = new Ersetzer(this.text);
		Sonderzeichen sonderzeichen = new Sonderzeichen();
		HashMap s = sonderzeichen.getSonderzeichen();
		this.text = ersetzer_sonderzeichen.ersetze(s);
	}

	private void ausgabeText(String pfad_out) {
		Ausgabe ausgabe = new Ausgabe(this.text);
		ausgabe.ausgabeKonsole();
		ausgabe.ausgabeDatei(pfad_out);
	}
}
