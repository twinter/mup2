package oop_praktikum.task3;

import java.util.HashMap;

/**
 * Klasse mit Sonderzeichen
 */
class Sonderzeichen {

	/**
	 * Funktion mit Sonderzeichen
	 *
	 * @return HashMap mit Sonderzeichen
	 */
	HashMap getSonderzeichen() {
		HashMap<String, String> sonderzeichen = new HashMap<>();
		sonderzeichen.put("&auml;", "ä");
		sonderzeichen.put("&Auml;", "Ä");
		sonderzeichen.put("&ouml;", "ö");
		sonderzeichen.put("&Ouml;", "Ö");
		sonderzeichen.put("&uuml;", "ü");
		sonderzeichen.put("&Uuml;", "Ü");
		sonderzeichen.put("&szlig;", "ß");
		sonderzeichen.put("&lt;", "<");
		sonderzeichen.put("&gt;", ">");
		sonderzeichen.put("&amp;", "&");
		sonderzeichen.put("&quot;", "ä");
		sonderzeichen.put("&nbsp;", "  ");
		return sonderzeichen;
	}
}
