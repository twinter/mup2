package oop_praktikum.task3;

/**
 * Entfernt die Tags in gegebenem Text
 */
class EntfernerTags {
	/**
	 * Der Text mit Tags
	 */
	private String text;

	/**
	 * Konstruktor
	 *
	 * @param text zu bearbeitender Text
	 */
	EntfernerTags(String text) {
		this.text = text;
	}

	/**
	 * Entfernt alle tags (inklusive Kommentare)
	 * Übersicht über RegEx-Patterns: https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
	 *
	 * @return Eingabetext ohne Tags
	 */
	String entferneTags() {

		// remove comments
		this.text = this.text.replaceAll("(?s)<!--.*?-->", "");

		// remove style, head and script tags
		this.text = this.text.replaceAll("(?s)<(style|head|script).*?(?:\".*?\".*?)*>.*?</\\1>", "");

		// remove tags (including attributes)
		this.text = this.text.replaceAll("<(?s)[!/]?.*?(?:\".*?\".*?)*>", "");

		return this.text;
	}
}
