package oop_praktikum.task5;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.util.ResourceBundle;

import static java.lang.Math.abs;

/**
 * Controller für das Fenster
 */
public class Fenster implements Initializable {
	/**
	 * Grenzen der Werte für automatische Skalierung
	 */
	private double bound_x = 2.7;
	private double bound_y = 10.;
	private double bound_border = 5;

	/**
	 * iteratives Funktionensystem
	 */
	private IFS ifs = null;

	/**
	 * Objekt für Hauptfensters
	 */
	private Stage stage = null;

	/**
	 * Zeichenfläche
	 */
	@FXML
	public Canvas canvas;

	/**
	 * Parent des Canvas, für automatische Größenanpasung
	 */
	@FXML
	public AnchorPane canvas_parent;

	/**
	 * Auswahl ob des Farbschema
	 */
	@FXML
	private RadioButton farben_einzeln;

	/**
	 * erste Farbe
	 */
	@FXML
	private ColorPicker farbe_1;

	/**
	 * zweite Farbe
	 */
	@FXML
	private ColorPicker farbe_2;

	/**
	 * dritte Farbe
	 */
	@FXML
	private ColorPicker farbe_3;

	/**
	 * vierte Farbe
	 */
	@FXML
	private ColorPicker farbe_4;

	/**
	 * eingabe der Anzahl der Iterationen
	 */
	@FXML
	private TextField iterationen;

	/**
	 * initialisiert das Fenster
	 *
	 * @param url URL
	 * @param resourceBundle resourceBundle
	 */
	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		this.farbe_1.setValue(Color.BLACK);
		this.farbe_2.setValue(Color.RED);
		this.farbe_3.setValue(Color.GREEN);
		this.farbe_4.setValue(Color.BLUE);

		// set text formatter to restrict inputs to integers
		iterationen.setTextFormatter(
			new TextFormatter<>(
				c -> {
					if (c.getControlNewText().isEmpty()) {
						return c;
					}

					DecimalFormat format = new DecimalFormat("0");
					ParsePosition parsePosition = new ParsePosition(0);
					Object object = format.parse(c.getControlNewText(), parsePosition);

					if (object == null || parsePosition.getIndex() < c.getControlNewText().length()) {
						return null;
					} else {
						return c;
					}
				}
			)
		);
	}

	/**
	 * setzt IFS
	 *
	 * @param ifs IFS
	 */
	void setIFS(IFS ifs) {
		this.ifs = ifs;
	}

	/**
	 * setzt Objekt für Hauptfenster
	 *
	 * @param stage Hauptfenster
	 */
	void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * entfernt Canvas und ermöglicht Verkleinerung
	 */
	@FXML
	void loeschePunkte() {
		this.canvas.setWidth(0);
		this.canvas.setHeight(0);
	}

	/**
	 * zeichnet Punkte
	 */
	@FXML
	void zeichnePunkte() {
		if (this.ifs == null)
			throw new IllegalStateException("IFS not set");

		int n;
		try {
			n = Math.round(Float.parseFloat(this.iterationen.getCharacters().toString()));
		} catch (NumberFormatException e) {
			System.out.println("You have to supply an integer");
			return;
		}

		// set everything up
		this.ifs.reset();
		GraphicsContext gc = this.canvas.getGraphicsContext2D();

		this.canvas.setWidth(this.canvas_parent.getWidth());
		this.canvas.setHeight(this.canvas_parent.getHeight());

		double width = gc.getCanvas().getWidth();
		double height = gc.getCanvas().getHeight();
		gc.clearRect(0, 0, width, height);

		double scale_factor = (width - 2 * this.bound_border) / this.bound_x;
		double scale_factor_y = (height - 2 * this.bound_border) / this.bound_y;
		if (scale_factor > scale_factor_y)
			scale_factor = scale_factor_y;

		// draw the points
		for (int i = 0; i < n; i++) {
			Punkt p = this.ifs.erzeugePunkt();

			String color;
			if (this.farben_einzeln.selectedProperty().getValue()) {
				color = String.valueOf(this.farbe_1.getValue());
			} else {
				switch (this.ifs.letzteZeile) {
					case 3:
						color = String.valueOf(this.farbe_4.getValue());
						break;
					case 2:
						color = String.valueOf(this.farbe_3.getValue());
						break;
					case 1:
						color = String.valueOf(this.farbe_2.getValue());
						break;
					default:
						color = String.valueOf(this.farbe_1.getValue());
						break;

				}
			}

			if (abs(p.x) > this.bound_x) {
				this.bound_x = abs(p.x);
				System.out.println("x boundary was too small, updated to " + Double.toString(this.bound_x));
			}
			if (abs(p.y) > this.bound_y) {
				this.bound_y = abs(p.y);
				System.out.println("y boundary was too small, updated to " + Double.toString(this.bound_y));
			}

			gc.setFill(Paint.valueOf(color));
			double x = p.x * scale_factor + width / 2;
			double y = height - p.y * scale_factor - this.bound_border;
			gc.fillOval(x, y, 1.25, 1.25);
		}
	}

	/**
	 * exportiert Bild
	 */
	@FXML
	private void exportiereBild() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Bild speichern");
		fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("PNG-Dateien", "*.png"));
		String initialPath = Paths.get("").toAbsolutePath().toString() + "/src/oop_praktikum/task5/";
		fileChooser.setInitialDirectory(new File(initialPath));
		fileChooser.setInitialFileName("Farn.png");
		File file = fileChooser.showSaveDialog(this.stage);
		if (file != null) {
			try {
				WritableImage snapshot = this.canvas.snapshot(null, null);
				BufferedImage image = SwingFXUtils.fromFXImage(snapshot, null);
				ImageIO.write(image, "png", file);
			} catch (IOException e) {
				System.out.println("Fehler beim schreiben der Datei");
			}
		}
	}
}
