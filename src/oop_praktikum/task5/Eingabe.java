package oop_praktikum.task5;


import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;

/**
 * eingeben der Datei
 * lesen der detei
 * lesen jede zeil
 * Jeder Wert in jeder zeile in array[][] speichern
 */
class Eingabe {

	/**
	 * pfadbasis ist pfad der datei
	 */
	private String pfadBasis;

	/**
	 * br ist eingabe der Tabelle vom benutzer zum lesen
	 */
	private BufferedReader br;

	/**
	 * Konstruktor
	 */
	Eingabe() {

		this.br = new BufferedReader(new InputStreamReader(System.in));
		this.pfadBasis = Paths.get("").toAbsolutePath().toString() + "/src/oop_praktikum/task5/";

	}

	/**
	 * einfach ,damit wir tabelle bekommen,Datentyp ist list[][] vom Float
	 *
	 * @return list[][] tabelle-werte
	 * @throws IOException    bei dateifehlern
	 * @throws ValueException bei wertefehlern
	 */
	Float[][] ladeTabelle() throws IOException, ValueException {

		System.out.println("Name der csv-Tabelle (ohne .csv am Ende) eingeben");
		String pfad = this.br.readLine() + ".csv";
		pfad = this.pfadBasis + pfad;
		BufferedReader fileReader = new BufferedReader(new FileReader(pfad));

		Float[][] tabelle = new Float[4][7];
		for (int i = 0; i < 4; i++) {
			String zeile = fileReader.readLine();
			String[] tabelle_string = zeile.split("[ \\t]+");

			if (tabelle_string.length < 7)
				throw new ValueException("zu wenig Elemente auf Zeile!");

			for (int j = 0; j < 7; j++) {
				tabelle[i][j] = Float.parseFloat(tabelle_string[j]);
			}
		}

		return tabelle;
	}
}
