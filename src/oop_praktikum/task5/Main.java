package oop_praktikum.task5;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main-Klasse
 */
public class Main extends Application {

	/**
	 * Fenster-Funktion
	 *
	 * @param args String-Array mit Argumenten
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * method to call the startup routines from Application
	 *
	 * @param primaryStage the primary window
	 * @throws Exception just in case
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fenster.fxml"));
		Parent root = fxmlLoader.load();

		Steuerung steuerung = new Steuerung(fxmlLoader.getController(), primaryStage);
		steuerung.start();

		primaryStage.setTitle("OOP task5");
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}


}
