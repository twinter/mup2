
package oop_praktikum.task5;

import java.util.Random;

/**
 * erzeugung einer zufaelligem punkt  von der tabelle
 */
class IFS {
	/**
	 * random vom typ Random
	 */
	private Random random;

	/**
	 * Punkt ist letzter punkt
	 */
	private Punkt letzterPunkt;

	/**
	 * letzte zeil,wo der punkt ist
	 */
	int letzteZeile = 0;

	/**
	 * tabelle ist Name-Tabelle,die gelesen wurde.
	 */
	private Float[][] tabelle;

	/**
	 * konstruktur
	 *
	 * @param tabelle ist Name-Tabelle,die gelesen wurde,vom benutzer abgegeben wurde
	 */
	IFS(Float[][] tabelle) {
		this.random = new Random();
		this.tabelle = tabelle;
		this.letzterPunkt = new Punkt(0, 0);
	}

	/**
	 * erzeugung einer zufaelligem punkt  von der tabelle
	 * @return neuer Punkt
	 */
	Punkt erzeugePunkt() {
		int zeile = 0;
		float r = random.nextFloat() - this.tabelle[0][6];
		while (r > 0) {
			zeile += 1;
			r -= tabelle[zeile][6];
		}

		float a = this.tabelle[zeile][0];
		float b = this.tabelle[zeile][1];
		float c = this.tabelle[zeile][2];
		float d = this.tabelle[zeile][3];
		float e = this.tabelle[zeile][4];
		float f = this.tabelle[zeile][5];
		float x = this.letzterPunkt.x;
		float y = this.letzterPunkt.y;

		float x_neu = a * x + b * y + e;
		float y_neu = c * x + d * y + f;

		this.letzterPunkt = new Punkt(x_neu, y_neu);
		this.letzteZeile = zeile;

		return this.letzterPunkt;
	}

	/**
	 * züruck zu erster punkt
	 */
	void reset() {
		this.letzterPunkt = new Punkt(0, 0);
	}
}
