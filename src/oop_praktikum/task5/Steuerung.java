package oop_praktikum.task5;

import javafx.stage.Stage;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

import java.io.IOException;

/**
 * Klasse mit grundlegender Programmlogik
 */
public class Steuerung {
	/**
	 * Controller-Klasse
	 */
	private Fenster fenster;

	/**
	 * Objekt für das Hauptfenster
	 */
	private Stage stage;

	/**
	 * Konstruktor
	 *
	 * @param controller Controller für das Hauptfenster
	 * @param stage Hauptfenster
	 */
	Steuerung(Fenster controller, Stage stage) {
		this.fenster = controller;
		this.stage = stage;
	}

	/**
	 * initialisiert Constroller
	 *
	 * @throws IOException fängt fehler in Dateieingabe ab
	 */
	void start() throws IOException {
		Eingabe eingabe = new Eingabe();
		Float[][] tabelle = null;
		try {
			tabelle = eingabe.ladeTabelle();
		} catch (IOException e) {
			System.out.println("Fehler bei Dateioperation");
			System.exit(10);
		} catch (ValueException e) {
			System.out.println(e.getMessage());
			System.exit(10);
		}
		IFS ifs = new IFS(tabelle);
		this.fenster.setIFS(ifs);
		this.fenster.setStage(stage);
	}
}
